package models

import (
	"errors"
	"time"

	"github.com/gofrs/uuid"
	"gopkg.in/vansante/go-ffprobe.v2"
)

var (
	ErrNoRecord = errors.New("models: no matching record found")
)

type MediaPackage struct {
	ID        int64        `json:"id"`
	Owner     string       `json:"owner"`
	Name      string       `json:"name"`
	GlobalID  uuid.UUID    `json:"global_id"`
	CreatedAt time.Time    `json:"created_at"`
	Status    string       `json:"status"`
	FolderID  *int         `json:"folder_id"`
	Poster    *MediaItem   `json:"poster,omitempty"`
	Folder    *MediaFolder `json:"folder,omitempty"`
	Media     *MediaItem   `json:"media,omitempty"`
}

type MediaItem struct {
	ID          int64
	PackageID   int64
	GlobalID    uuid.UUID
	CreatedAt   time.Time
	TypeCode    string
	FileExt     string
	MetaDataRaw string
	MetaData    struct {
		Media *ffprobe.ProbeData `json:"media,omitempty"`
	}
}

type MediaFolder struct {
	ID        int       `json:"id"`
	Owner     string    `json:"owner"`
	Name      string    `json:"name"`
	GlobalID  uuid.UUID `json:"global_id"`
	CreatedAt time.Time `json:"created_at"`
}
