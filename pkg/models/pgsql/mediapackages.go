package pgsql

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/gofrs/uuid"
	"zenogre.net/phoenix/uploader/pkg/models"

	"github.com/jackc/pgx/v4"
)

const (
	qryInsertAudit = `INSERT INTO media.audit_%s (user_ref, created_at, action, data) 
	SELECT $1 AS user_ref, NOW() AS created_at, $3 AS action, row_to_json(%s) AS data FROM media.%s WHERE id = $2`

	qryGetPackages = `SELECT p.id, p.owner, p.name, p.created_at, p.global_id, p.status, p.folder_id 
	FROM media.packages AS p WHERE %s`

	/*
		qryGetPackagesExt = `SELECT DISTINCT ON (p.id)
		p.id, p.owner, p.name, p.created_at, p.global_id, p.status, p.folder_id,
		i_poster.id AS ip_id, i_poster.global_id AS ip_global_id, i_poster.file_ext as ip_file_ext
		FROM media.packages AS p
		LEFT JOIN media.items AS i_poster ON i_poster.package_id = p.id AND i_poster.type_code='IMAGE_POSTER'
		WHERE %s ORDER BY p.id DESC, i_poster.id DESC`
	*/

	qryLockPackage = `UPDATE media.packages SET fs_lock=NOW()
	WHERE (fs_lock IS NULL OR fs_lock < (NOW() - INTERVAL '1 hour')) AND id=$1 RETURNING fs_lock`

	qryUnlockPackage = `UPDATE media.packages set fs_lock=NULL 
	WHERE fs_lock= $1 AND id=$2`

	qryInsertPackage = `INSERT INTO media.packages (owner, name, created_at, global_id, status)
		VALUES($1, $2, NOW(), $3, $4) RETURNING id`

	qryUpdatePackage = `UPDATE media.packages SET name = $3, folder_id = $4, status = $5
	WHERE id=$1 AND status=$2`

	qryDeletePackage = `DELETE FROM media.packages
	WHERE id = $1`

	qryInsertItem = `INSERT INTO media.items (package_id, created_at, global_id, type_code, file_ext, metadata)
		VALUES ($1, NOW(), $2, $3, $4, $5) RETURNING id`

	qryGetItems = `SELECT i.id, i.package_id, i.created_at, i.global_id, i.type_code, i.file_ext, i.metadata 
		FROM media.items AS i %s WHERE %s`

	qryDeleteItem = `DELETE FROM media.items WHERE id = $1`

	qryGetFolders = `SELECT id, owner, name, created_at, global_id FROM media.folders WHERE `

	qryInsertFolder = `INSERT INTO media.folders (owner, name, created_at, global_id)
		VALUES($1, $2, NOW(), $3) RETURNING id`

	qryUpdateFolder = `UPDATE media.folders SET name = $2
		WHERE id=$1`

	qryDeleteFolder = `DELETE FROM media.folders
		WHERE id = $1`
)

func (m *DBModel) New(file *models.MediaPackage, item *models.MediaItem) error {
	pkgGID, err := uuid.NewV4()
	if err != nil {
		return err
	}

	tx, err := m.db.Begin(context.Background())
	if err != nil {
		return err
	}

	defer tx.Rollback(context.Background()) //nolint

	var fileID int64
	err = m.db.QueryRow(context.Background(), qryInsertPackage,
		file.Owner,
		file.Name,
		pkgGID,
		file.Status,
	).Scan(&fileID)

	if err != nil {
		return err
	}

	err = m.audit("packages", "INSERT", fileID)
	if err != nil {
		return err
	}

	if item != nil {
		item.PackageID = fileID
		err := m.NewItem(item)
		if err != nil {
			item.PackageID = 0
			return err
		}
	}

	err = tx.Commit(context.Background())
	if err != nil {
		if item != nil {
			item.PackageID = 0
		}

		return err
	}

	file.ID = fileID
	file.GlobalID = pkgGID

	return nil
}

func (m *DBModel) Get(searchQuery string, qryArgs ...interface{}) ([]*models.MediaPackage, error) {
	rows, err := m.db.Query(context.Background(), fmt.Sprintf(qryGetPackages, searchQuery), qryArgs...)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	pkgs := []*models.MediaPackage{}

	for rows.Next() {
		p := &models.MediaPackage{}

		err = rows.Scan(
			&p.ID,
			&p.Owner,
			&p.Name,
			&p.CreatedAt,
			&p.GlobalID,
			&p.Status,
			&p.FolderID,
		)

		if err != nil {
			return nil, err
		}

		pkgs = append(pkgs, p)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return pkgs, nil
}

func (m *DBModel) GetByID(id interface{}) (*models.MediaPackage, error) {
	var searchField string

	switch v := id.(type) {
	case int64:
		searchField = "p.id"
	case uuid.UUID:
		searchField = "p.global_id"
	default:
		return nil, fmt.Errorf("unknown id type %T", v)
	}

	files, err := m.Get(fmt.Sprintf("%s = $1 AND p.status != 'DELETE'", searchField), id)
	if err != nil {
		return nil, err
	}

	if len(files) < 1 {
		return nil, models.ErrNoRecord
	}

	return files[0], nil
}

func (m *DBModel) GetByOwner(owner string) ([]*models.MediaPackage, error) {
	return m.Get("p.owner = $1 AND p.status != 'DELETE'", owner)
}

func (m *DBModel) GetDelete() ([]*models.MediaPackage, error) {
	return m.Get("p.status = 'DELETE'")
}

func (m *DBModel) UpdateFile(file *models.MediaPackage, stateLimit string) error {
	commandTag, err := m.db.Exec(context.Background(), qryUpdatePackage, file.ID, stateLimit, file.Name, file.FolderID, file.Status)
	if err != nil {
		return err
	}

	if commandTag.RowsAffected() != 1 {
		return errors.New("No row found to update")
	}

	err = m.audit("packages", "UPDATE", file.ID)
	if err != nil {
		return err
	}

	return nil
}

func (m *DBModel) DeleteFile(file *models.MediaPackage) error {
	err := m.audit("packages", "DELETE", file.ID)
	if err != nil {
		return err
	}

	commandTag, err := m.db.Exec(context.Background(), qryDeletePackage, file.ID)
	if err != nil {
		return err
	}

	if commandTag.RowsAffected() != 1 {
		return errors.New("No row found to delete")
	}

	return nil
}

func (m *DBModel) Lock(fileID int64) (*time.Time, error) {
	var fsLock *time.Time

	err := m.db.QueryRow(context.Background(), qryLockPackage, fileID).Scan(&fsLock)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}

		return nil, err
	}

	return fsLock, nil
}

func (m *DBModel) Unlock(fsLock *time.Time, fileID int64) error {

	commandTag, err := m.db.Exec(context.Background(), qryUnlockPackage, fsLock, fileID)
	if err != nil {
		return err
	}

	if commandTag.RowsAffected() != 1 {
		return fmt.Errorf("unable to unlock package %v with fs_lock %v", fileID, fsLock)
	}

	return nil
}

func (m *DBModel) NewItem(item *models.MediaItem) error {
	itemGID, err := uuid.NewV4()
	if err != nil {
		return err
	}

	var itemID int64
	err = m.db.QueryRow(context.Background(), qryInsertItem,
		item.PackageID,
		itemGID,
		item.TypeCode,
		item.FileExt,
		item.MetaDataRaw,
	).Scan(&itemID)

	if err != nil {
		return err
	}

	err = m.audit("items", "INSERT", itemID)
	if err != nil {
		return err
	}

	item.GlobalID = itemGID

	return nil
}

func (m *DBModel) ItemGet(searchTarget string, searchQuery string, qryArgs ...interface{}) ([]*models.MediaItem, error) {
	rows, err := m.db.Query(context.Background(),
		fmt.Sprintf(qryGetItems, searchTarget, searchQuery), qryArgs...)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	items := []*models.MediaItem{}

	for rows.Next() {
		i := &models.MediaItem{}
		err = rows.Scan(
			&i.ID,
			&i.PackageID,
			&i.CreatedAt,
			&i.GlobalID,
			&i.TypeCode,
			&i.FileExt,
			&i.MetaData,
		)

		if err != nil {
			return nil, err
		}

		items = append(items, i)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return items, nil
}

func (m *DBModel) GetItemByID(id interface{}) (*models.MediaItem, error) {
	var searchField string

	switch v := id.(type) {
	case int64:
		searchField = "i.id"
	case uuid.UUID:
		searchField = "i.global_id"
	default:
		return nil, fmt.Errorf("unknown id type %T", v)
	}

	items, err := m.ItemGet("", fmt.Sprintf("%s = $1", searchField), id)
	if err != nil {
		return nil, err
	}

	if len(items) < 1 {
		return nil, models.ErrNoRecord
	}

	return items[0], nil
}

func (m *DBModel) GetItemsByPackage(pkgID int64) ([]*models.MediaItem, error) {
	items, err := m.ItemGet("", "i.package_id = $1", pkgID)
	if err != nil {
		return nil, err
	}

	return items, nil
}

func (m *DBModel) GetItemsByPackageAndType(pkgID int64, itemType string) ([]*models.MediaItem, error) {
	items, err := m.ItemGet("", "i.package_id = $1 AND i.type_code = $2", pkgID, itemType)
	if err != nil {
		return nil, err
	}

	return items, nil
}

func (m *DBModel) GetItemsByOwner(owner string) ([]*models.MediaItem, error) {
	items, err := m.ItemGet("INNER JOIN media.packages AS p ON p.id=i.package_id", "p.owner = $1", owner)
	if err != nil {
		return nil, err
	}

	return items, nil
}

func (m *DBModel) DeleteItem(item *models.MediaItem) error {
	err := m.audit("items", "DELETE", item.ID)
	if err != nil {
		return err
	}

	commandTag, err := m.db.Exec(context.Background(), qryDeleteItem, item.ID)
	if err != nil {
		return err
	}

	if commandTag.RowsAffected() != 1 {
		return errors.New("No row found to delete")
	}

	return nil
}

func (m *DBModel) FolderGet(searchQuery string, qryArgs ...interface{}) ([]*models.MediaFolder, error) {
	rows, err := m.db.Query(context.Background(), qryGetFolders+searchQuery, qryArgs...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	folders := []*models.MediaFolder{}

	for rows.Next() {
		f := &models.MediaFolder{}
		err = rows.Scan(
			&f.ID,
			&f.Owner,
			&f.Name,
			&f.CreatedAt,
			&f.GlobalID,
		)

		if err != nil {
			return nil, err
		}

		folders = append(folders, f)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return folders, nil
}

func (m *DBModel) GetFolderByID(id interface{}) (*models.MediaFolder, error) {
	var searchField string

	switch v := id.(type) {
	case int:
		searchField = "id"
	case uuid.UUID:
		searchField = "global_id"
	default:
		return nil, fmt.Errorf("unknown id type %T", v)
	}

	folders, err := m.FolderGet(fmt.Sprintf("%s = $1", searchField), id)
	if err != nil {
		return nil, err
	}

	if len(folders) < 1 {
		return nil, models.ErrNoRecord
	}

	return folders[0], nil
}

func (m *DBModel) GetFoldersByOwner(owner string) ([]*models.MediaFolder, error) {
	return m.FolderGet("owner = $1", owner)
}

func (m *DBModel) NewFolder(folder *models.MediaFolder) error {
	folderGID, err := uuid.NewV4()
	if err != nil {
		return err
	}

	var folderID int
	err = m.db.QueryRow(context.Background(), qryInsertFolder,
		folder.Owner,
		folder.Name,
		folderGID,
	).Scan(&folderID)

	if err != nil {
		return err
	}

	err = m.audit("folders", "INSERT", folderID)
	if err != nil {
		return err
	}

	folder.ID = folderID
	folder.GlobalID = folderGID

	return nil
}

func (m *DBModel) UpdateFolder(folder *models.MediaFolder) error {
	commandTag, err := m.db.Exec(context.Background(), qryUpdateFolder, folder.ID, folder.Name)
	if err != nil {
		return err
	}

	if commandTag.RowsAffected() != 1 {
		return errors.New("No row found to update")
	}

	err = m.audit("folders", "UPDATE", folder.ID)
	if err != nil {
		return err
	}

	return nil
}

func (m *DBModel) DeleteFolder(folder *models.MediaFolder) error {
	err := m.audit("folders", "DELETE", folder.ID)
	if err != nil {
		return err
	}

	commandTag, err := m.db.Exec(context.Background(), qryDeleteFolder, folder.ID)
	if err != nil {
		return err
	}

	if commandTag.RowsAffected() != 1 {
		return errors.New("No row found to delete")
	}

	return nil
}

func (m *DBModel) audit(table string, action string, id interface{}) error {
	commandTag, err := m.db.Exec(context.Background(), fmt.Sprintf(qryInsertAudit, table, table, table),
		m.AuditUser,
		id,
		action,
	)

	if err != nil {
		return err
	}

	if commandTag.RowsAffected() != 1 {
		return errors.New("No rows inserted")
	}

	return nil
}
