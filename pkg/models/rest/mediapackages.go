package rest

import (
	"encoding/base64"
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/render"
	"github.com/gofrs/uuid"
	"zenogre.net/phoenix/uploader/pkg/models"
	"zenogre.net/phoenix/uploader/pkg/util"
)

type MediaFolderResp struct {
	*models.MediaFolder
	ID       *uuid.UUID `json:"id"`
	GlobalID omit       `json:"global_id,omitempty"`
	Owner    omit       `json:"owner,omitempty"`
}

type MediaFolderReq struct {
	*models.MediaFolder
	ProtectedID    string `json:"id"`
	ProtectedGID   string `json:"global_id"`
	ProtectedOwner string `json:"owner"`
}

type MediaPackageResp struct {
	rm *RestModel
	*models.MediaPackage
	GlobalID        omit      `json:"global_id,omitempty"`
	Owner           omit      `json:"owner,omitempty"`
	FolderID        omit      `json:"folder_id,omitempty"`
	ID              uuid.UUID `json:"id"`
	Poster          string    `json:"poster"`
	Folder          string    `json:"folder"`
	Media           string    `json:"media,omitempty"`
	PosterSmall16x9 string    `json:"poster_sm16x9"`
	ShortID         string    `json:"short_id"`
	Duration        int64     `json:"duration"`
	Width           int       `json:"width"`
	Height          int       `json:"height"`
}

type MediaPackageReq struct {
	*models.MediaPackage
	ProtectedID     string `json:"id"`
	ProtectedGID    string `json:"global_id"`
	ProtectedOwner  string `json:"owner"`
	ProtectedPoster string `json:"poster"`
	ProtectedMedia  string `json:"media"`
	PosterBin       []byte `json:"poster_bin"`
	Folder          string `json:"folder"`
}

func (f *MediaFolderReq) Bind(r *http.Request) error {
	if f.MediaFolder == nil {
		return errors.New("Missing required fields.")
	}

	f.ProtectedID = ""
	f.ProtectedGID = ""
	f.ProtectedOwner = ""
	return nil
}

func (rm *RestModel) NewMediaFolderRespList(folders []*models.MediaFolder) []render.Renderer {
	list := []render.Renderer{}
	for _, folder := range folders {
		list = append(list, rm.NewMediaFolderResp(folder))
	}
	return list
}

func (rm *RestModel) NewMediaFolderResp(folder *models.MediaFolder) *MediaFolderResp {
	resp := &MediaFolderResp{
		MediaFolder: folder,
		ID:          &folder.GlobalID,
	}

	return resp
}

func (rd *MediaFolderResp) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (rm *RestModel) NewMediaPackageRespList(files []*models.MediaPackage) []render.Renderer {
	list := []render.Renderer{}
	for _, file := range files {
		list = append(list, rm.NewMediaPackageResp(file))
	}
	return list
}

func (rm *RestModel) NewMediaPackageResp(file *models.MediaPackage) *MediaPackageResp {
	resp := &MediaPackageResp{
		rm:           rm,
		MediaPackage: file,
	}

	return resp
}

func (resp *MediaPackageResp) Render(w http.ResponseWriter, r *http.Request) error {
	resp.ID = resp.MediaPackage.GlobalID
	resp.ShortID = base64.RawURLEncoding.EncodeToString(resp.MediaPackage.GlobalID.Bytes())

	if resp.MediaPackage.Folder != nil {
		resp.Folder = resp.MediaPackage.Folder.GlobalID.String()
	}

	if resp.MediaPackage.Media != nil {
		resp.Media = fmt.Sprintf("%s/%s/%s%s",
			resp.MediaPackage.Owner,
			resp.MediaPackage.GlobalID,
			resp.MediaPackage.Media.GlobalID,
			resp.MediaPackage.Media.FileExt,
		)

		if resp.MediaPackage.Media.MetaData.Media != nil {
			resp.Duration = resp.MediaPackage.Media.MetaData.Media.Format.Duration().Milliseconds()
			video := resp.MediaPackage.Media.MetaData.Media.FirstVideoStream()
			if video != nil {
				resp.Width = video.Width
				resp.Height = video.Height
			}
		}
	}

	if resp.MediaPackage.Poster != nil {
		posterURL := fmt.Sprintf("/%s/%s/%s%s",
			resp.MediaPackage.Owner,
			resp.MediaPackage.GlobalID,
			resp.MediaPackage.Poster.GlobalID.String(),
			resp.MediaPackage.Poster.FileExt,
		)

		posterURLSmall, _ := util.NewImageURL(
			posterURL,
			"fill",
			640,
			360,
			"sm",
			0,
			"jpg",
			resp.rm.conf.ImgProxy.Key,
			resp.rm.conf.ImgProxy.Salt,
		)
		resp.PosterSmall16x9 = resp.rm.conf.ImgProxy.BaseURL + posterURLSmall

		posterURLNorm, _ := util.NewImageURL(
			posterURL,
			"fit",
			3840,
			2160,
			"sm",
			0,
			"jpg",
			resp.rm.conf.ImgProxy.Key,
			resp.rm.conf.ImgProxy.Salt,
		)
		resp.Poster = resp.rm.conf.ImgProxy.BaseURL + posterURLNorm
	}

	return nil
}

func (f *MediaPackageReq) Bind(r *http.Request) error {
	if f.MediaPackage == nil {
		return errors.New("Missing required fields.")
	}

	f.ProtectedID = ""
	f.ProtectedGID = ""
	f.ProtectedOwner = ""
	f.ProtectedPoster = ""
	f.ProtectedMedia = ""
	return nil
}
