package rest

import (
	"zenogre.net/phoenix/uploader/pkg/util"
)

type RestModel struct {
	conf *util.Config
}

type omit *struct{}

func NewRestModel(conf *util.Config) *RestModel {
	return &RestModel{conf: conf}
}
