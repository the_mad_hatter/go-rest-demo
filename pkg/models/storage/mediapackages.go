package storage

import (
	"errors"
	"fmt"
	"io"
	"path"

	"zenogre.net/phoenix/uploader/pkg/models"
	"zenogre.net/phoenix/uploader/pkg/util"
)

func (sm *StorageModel) NewItem(file *models.MediaPackage, item *models.MediaItem, data io.ReadSeeker, sftp bool) error {
	err := util.S3Upload(
		data,
		fmt.Sprintf("%s/%s/%s/%s%s",
			sm.conf.AWS.BucketRoot,
			file.Owner,
			file.GlobalID.String(),
			item.GlobalID.String(),
			item.FileExt,
		),
		sm.conf.AWS.Bucket,
		sm.conf.AWS.Key,
		sm.conf.AWS.Secret,
	)

	if err != nil {
		return err
	}

	if !sftp {
		return nil
	}

	_, err = data.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	err = util.SFTPUpload(
		data,
		fmt.Sprintf("%s/%s/%s/%s%s",
			sm.conf.SFTP.Root,
			file.Owner,
			file.GlobalID.String(),
			item.GlobalID.String(),
			item.FileExt),
		sm.conf.SFTP.Host,
		sm.conf.SFTP.User,
		sm.conf.SFTP.MyKey,
		sm.conf.SFTP.Keys,
	)
	if err != nil {
		return err
	}

	return nil
}

func (sm *StorageModel) DeletePackage(file *models.MediaPackage) error {
	filePath := path.Clean(fmt.Sprintf("%s/%s/%s",
		sm.conf.SFTP.Root,
		file.Owner,
		file.GlobalID.String(),
	))

	ok, err := path.Match("*/*/*", filePath)
	if err != nil {
		return err
	}

	if !ok {
		return errors.New("file path anomaly detected")
	}

	err = util.SFTPDeleteAll(
		filePath,
		sm.conf.SFTP.Host,
		sm.conf.SFTP.User,
		sm.conf.SFTP.MyKey,
		sm.conf.SFTP.Keys,
	)
	if err != nil {
		return err
	}

	filePath = path.Clean(fmt.Sprintf("%s/%s/%s",
		sm.conf.AWS.BucketRoot,
		file.Owner,
		file.GlobalID.String(),
	))

	ok, err = path.Match("*/*/*", filePath)
	if err != nil {
		return err
	}

	if !ok {
		return errors.New("file path anomaly detected")
	}

	filePath += "/"

	err = util.S3Delete(
		filePath,
		sm.conf.AWS.Bucket,
		sm.conf.AWS.Key,
		sm.conf.AWS.Secret,
	)

	if err != nil {
		return err
	}

	return nil
}

func (sm *StorageModel) DeleteItem(file *models.MediaPackage, item *models.MediaItem) error {
	filePath := path.Clean(fmt.Sprintf("%s/%s/%s/%s%s",
		sm.conf.AWS.BucketRoot,
		file.Owner,
		file.GlobalID.String(),
		item.GlobalID.String(),
		item.FileExt,
	))

	ok, err := path.Match("*/*/*/*", filePath)
	if err != nil {
		return err
	}

	if !ok {
		return errors.New("file path anomaly detected")
	}

	err = util.S3Delete(
		filePath,
		sm.conf.AWS.Bucket,
		sm.conf.AWS.Key,
		sm.conf.AWS.Secret,
	)

	if err != nil {
		return err
	}

	return nil
}
