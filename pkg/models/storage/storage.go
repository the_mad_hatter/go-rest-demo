package storage

import "zenogre.net/phoenix/uploader/pkg/util"

type StorageModel struct {
	conf *util.Config
}

func NewStorageModel(conf *util.Config) *StorageModel {
	return &StorageModel{conf: conf}
}
