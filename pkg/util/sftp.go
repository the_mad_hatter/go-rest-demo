package util

import (
	"fmt"
	"io"
	"io/ioutil"
	"path"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	kh "golang.org/x/crypto/ssh/knownhosts"
)

// Build and return an ssh compatible config.
func sftpConfig(pkey string, hkeys string, user string) (*ssh.ClientConfig, error) {
	key, err := ioutil.ReadFile(pkey)
	if err != nil {
		return nil, fmt.Errorf("unable to read private key: %w", err)
	}

	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		return nil, fmt.Errorf("unable to parse private key: %w", err)
	}

	hostKeyCallback, err := kh.New(hkeys)
	if err != nil {
		return nil, fmt.Errorf("could not create hostkeycallback function: %w", err)
	}

	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: hostKeyCallback,
	}

	return config, nil
}

// Upload a file to the specified destination creating necessary intermediate directories.
func SFTPUpload(srcFile io.Reader, dest string, remote string, user string, pkey string, hkeys string) error {
	port := ":22"

	config, err := sftpConfig(pkey, hkeys, user)
	if err != nil {
		return err
	}

	conn, err := ssh.Dial("tcp", remote+port, config)
	if err != nil {
		return err
	}

	defer conn.Close()

	client, err := sftp.NewClient(conn)
	if err != nil {
		return err
	}

	defer client.Close()

	err = client.MkdirAll(path.Dir(dest))
	if err != nil {
		return err
	}

	dstFile, err := client.Create(dest)
	if err != nil {
		return err
	}

	defer dstFile.Close()

	_, err = io.Copy(dstFile, srcFile)
	if err != nil {
		return err
	}

	return nil
}

// Delete directory and all the files within.
func SFTPDeleteAll(delPath string, remote string, user string, pkey string, hkeys string) error {
	port := ":22"

	config, err := sftpConfig(pkey, hkeys, user)
	if err != nil {
		return err
	}

	conn, err := ssh.Dial("tcp", remote+port, config)
	if err != nil {
		return err
	}

	defer conn.Close()

	client, err := sftp.NewClient(conn)
	if err != nil {
		return err
	}

	defer client.Close()

	files, err := client.ReadDir(delPath)
	if err != nil {
		return err
	}

	for _, file := range files {
		err = client.Remove(path.Join(delPath, file.Name()))
		if err != nil {
			return err
		}
	}
	err = client.RemoveDirectory(delPath)
	if err != nil {
		return err
	}

	return nil
}
