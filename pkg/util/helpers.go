package util

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/jackc/pgtype"
	pgtypeuuid "github.com/jackc/pgtype/ext/gofrs-uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gopkg.in/vansante/go-ffprobe.v2"
)

type Config struct {
	DestDir string
	DSN     string
	APIUrl  string
	AWS     struct {
		Key        string
		Secret     string
		Bucket     string
		BucketRoot string
	}
	SFTP struct {
		Host  string
		User  string
		MyKey string
		Keys  string
		Root  string
	}
	AdminIPS []string
	ImgProxy struct {
		BaseURL string
		Salt    string
		Key     string
	}
}

type authValid struct {
	Valid bool `json:"valid"`
}

func OpenDB(dsn string) (*pgx.Conn, error) {
	db, err := pgx.Connect(context.Background(), dsn)
	if err != nil {
		return nil, err
	}

	err = dbAfterConnect(context.Background(), db)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func OpenDBPool(dsn string) (*pgxpool.Pool, error) {
	dbconfig, err := pgxpool.ParseConfig(dsn)
	if err != nil {
		return nil, err
	}

	dbconfig.AfterConnect = dbAfterConnect

	dbpool, err := pgxpool.ConnectConfig(context.Background(), dbconfig)
	if err != nil {
		return nil, err
	}

	return dbpool, nil
}

// Map pg UUID type.
func dbAfterConnect(ctx context.Context, conn *pgx.Conn) error {
	conn.ConnInfo().RegisterDataType(pgtype.DataType{
		Value: &pgtypeuuid.UUID{},
		Name:  "uuid",
		OID:   pgtype.UUIDOID,
	})
	return nil
}

// Validate user/pass with auth api.
func DoValidateFolder(apiURL string, user string, pass string) (bool, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", apiURL, nil)

	if err != nil {
		return false, err
	}

	req.SetBasicAuth(user, pass)

	resp, err := client.Do(req)
	if err != nil {
		return false, err
	}

	defer resp.Body.Close()

	var result authValid

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return false, err
	}

	return result.Valid, nil
}

// Load JSON formatted config file into a config object.
func LoadConfig(file string) (*Config, error) {
	var conf Config
	configFile, err := os.Open(file)

	defer configFile.Close()
	if err != nil {
		return nil, fmt.Errorf("cannot load config: %w", err)
	}

	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&conf)
	return &conf, nil
}

// Given requested image specs create the url to our backend image server.
func NewImageURL(path string, resize string, width int, height int, gravity string, enlarge int, extension string, key string, salt string) (string, error) {
	var keyBin, saltBin []byte
	var err error

	if keyBin, err = hex.DecodeString(key); err != nil {
		return "", fmt.Errorf("Key expected to be hex-encoded string: %w", err)
	}

	if saltBin, err = hex.DecodeString(salt); err != nil {
		return "", fmt.Errorf("Salt expected to be hex-encoded string: %w", err)
	}

	encPath := base64.RawURLEncoding.EncodeToString([]byte(path))
	cmd := fmt.Sprintf("/%s/%d/%d/%s/%d/%s.%s", resize, width, height, gravity, enlarge, encPath, extension)

	mac := hmac.New(sha256.New, keyBin)
	mac.Write(saltBin)
	mac.Write([]byte(cmd))
	sig := base64.RawURLEncoding.EncodeToString(mac.Sum(nil))
	out := fmt.Sprintf("/%s%s", sig, cmd)
	return out, nil
}

// Open file and hand off to ffprobe method.
func ProbeUploadFile(probeFile string) (string, error) {
	fileReader, err := os.Open(probeFile)
	if err != nil {
		return "", fmt.Errorf("Error opening probe file: %w", err)
	}
	defer fileReader.Close()

	data, err := ProbeUpload(fileReader)
	if err != nil {
		return "", err
	}

	return data, nil
}

// Probe the file with ffprobe for media metadata.
func ProbeUpload(fileReader io.Reader) (string, error) {
	ctx, cancelFn := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelFn()

	data, err := ffprobe.ProbeReader(ctx, fileReader)
	if err != nil {
		return "", fmt.Errorf("Error getting data: %w", err)
	}

	buf, err := json.Marshal(data)
	if err != nil {
		return "", fmt.Errorf("Error unmarshalling: %w", err)
	}

	return string(buf), nil
}
