package manager

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"io"

	"zenogre.net/phoenix/uploader/pkg/models"
	"zenogre.net/phoenix/uploader/pkg/util"
)

func (m *Manager) UpdateMediaPackagePoster(file *models.MediaPackage, imgReader io.ReadSeeker) error {
	_, format, err := image.DecodeConfig(imgReader)
	if err != nil {
		return err
	}

	_, err = imgReader.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	var imgMeta string

	if format != "jpeg" {

		img, _, err := image.Decode(imgReader)
		if err != nil {
			return err
		}

		jpegImg := new(bytes.Buffer)
		err = jpeg.Encode(jpegImg, img, &jpeg.Options{Quality: 90})
		if err != nil {
			return err
		}

		imgReader = bytes.NewReader(jpegImg.Bytes())
		imgMeta, err = util.ProbeUpload(imgReader)
		if err != nil {
			return err
		}

		_, err = imgReader.Seek(0, io.SeekStart)
		if err != nil {
			return err
		}

	} else {

		imgMeta, err = util.ProbeUpload(imgReader)
		if err != nil {
			return err
		}

		_, err = imgReader.Seek(0, io.SeekStart)
		if err != nil {
			return err
		}
	}

	fsLock, err := m.db.Lock(file.ID)
	if err != nil {
		return err
	}

	if fsLock == nil {
		return fmt.Errorf("unable to get edit lock on package %v", file.ID)
	}

	items, err := m.db.GetItemsByPackageAndType(file.ID, "IMAGE_POSTER")
	if err != nil {
		return err
	}

	for _, item := range items {
		err = m.sm.DeleteItem(file, item)
		if err != nil {
			return err
		}

		err = m.db.DeleteItem(item)
		if err != nil {
			return err
		}
	}

	item := &models.MediaItem{
		PackageID:   file.ID,
		TypeCode:    "IMAGE_POSTER",
		FileExt:     ".jpg",
		MetaDataRaw: fmt.Sprintf("{\"media\":%s}", imgMeta),
	}

	err = m.db.NewItem(item)
	if err != nil {
		return err
	}

	err = m.sm.NewItem(file, item, imgReader, false)
	if err != nil {
		return err
	}

	file.Poster = item

	err = m.db.Unlock(fsLock, file.ID)
	if err != nil {
		return err
	}

	return nil
}
