package manager

import (
	"zenogre.net/phoenix/uploader/pkg/models/pgsql"
	"zenogre.net/phoenix/uploader/pkg/models/storage"
)

type Manager struct {
	db *pgsql.DBModel
	sm *storage.StorageModel
}

func NewManager(db *pgsql.DBModel, sm *storage.StorageModel) *Manager {
	return &Manager{
		db: db,
		sm: sm,
	}
}
