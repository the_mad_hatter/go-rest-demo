package main

import (
	"bytes"
	"errors"
	"fmt"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"net/http"
	"unicode/utf8"

	"github.com/go-chi/render"
	"github.com/gofrs/uuid"
	"zenogre.net/phoenix/uploader/pkg/manager"
	"zenogre.net/phoenix/uploader/pkg/models"
	"zenogre.net/phoenix/uploader/pkg/models/rest"
	"zenogre.net/phoenix/uploader/pkg/models/storage"
)

func (app *application) home(w http.ResponseWriter, r *http.Request) {
	app.errNotFound(w, r)
}

func (app *application) getFile(w http.ResponseWriter, r *http.Request) {
	file := r.Context().Value(contextKeyFile).(*models.MediaPackage)

	file.Poster = nil

	items, err := app.files.GetItemsByPackage(file.ID)
	if err != nil {
		app.errServer(w, r, fmt.Errorf("unable to load items for package %v: %w", file.ID, err))
		return
	}

	for _, item := range items {
		switch item.TypeCode {
		case "IMAGE_POSTER":
			if file.Poster != nil {
				if item.CreatedAt.After(file.Poster.CreatedAt) {
					file.Poster = item
				}
			} else {
				file.Poster = item
			}

		case "MEDIA_ORIG":
			if file.Media != nil {
				if item.CreatedAt.After(file.Media.CreatedAt) {
					file.Media = item
				}
			} else {
				file.Media = item
			}
		}
	}
	rm := rest.NewRestModel(app.config)

	if err := render.Render(w, r, rm.NewMediaPackageResp(file)); err != nil {
		app.errServer(w, r, err)
		return
	}
}

func (app *application) updateFile(w http.ResponseWriter, r *http.Request) {
	file := r.Context().Value(contextKeyFile).(*models.MediaPackage)

	data := &rest.MediaPackageReq{MediaPackage: file}
	if err := render.Bind(r, data); err != nil {
		app.logError(err)
		app.errInvalidRequest(w, r, err)
		return
	}

	file = data.MediaPackage

	if utf8.RuneCountInString(file.Name) < 1 || utf8.RuneCountInString(file.Name) > 100 {
		app.errInvalidRequest(w, r, nil)
		return
	}

	if data.Folder == "" && file.FolderID != nil {
		file.FolderID = nil
		file.Folder = nil
	} else if data.Folder != "" {
		folderID, err := uuid.FromString(data.Folder)
		if err != nil {
			app.errInvalidRequest(w, r, nil)
			return
		}

		folder, err := app.files.GetFolderByID(folderID)
		if errors.Is(err, models.ErrNoRecord) {
			app.errInvalidRequest(w, r, nil)
			return
		} else if err != nil {
			app.errServer(w, r, err)
			return
		}

		auth := app.getAuth(r)
		if auth != nil {
			if folder.Owner != auth.owner {
				app.errUnauthorized(w, r, nil)
				return
			}
		}

		file.FolderID = new(int)
		*file.FolderID = folder.ID
		file.Folder = folder
	}

	err := app.files.UpdateFile(file, "READY")
	if err != nil {
		app.errServer(w, r, err)
		return
	}

	if len(data.PosterBin) > 0 {
		if len(data.PosterBin) > (10 * 1024 * 1024) {
			app.errInvalidRequest(w, r, nil)
			return
		}

		smodel := storage.NewStorageModel(app.config)
		manager := manager.NewManager(app.files, smodel)

		var imgReader io.ReadSeeker = bytes.NewReader(data.PosterBin)
		err = manager.UpdateMediaPackagePoster(file, imgReader)
		if err != nil {
			app.errServer(w, r, err)
			return
		}
	}

	rm := rest.NewRestModel(app.config)

	if err := render.Render(w, r, rm.NewMediaPackageResp(file)); err != nil {
		app.errServer(w, r, err)
		return
	}
}

func (app *application) deleteFile(w http.ResponseWriter, r *http.Request) {
	file := r.Context().Value(contextKeyFile).(*models.MediaPackage)

	file.Status = "DELETE"
	err := app.files.UpdateFile(file, "READY")
	if err != nil {
		app.errServer(w, r, err)
		return
	}

	rm := rest.NewRestModel(app.config)

	if err := render.Render(w, r, rm.NewMediaPackageResp(file)); err != nil {
		app.errServer(w, r, err)
		return
	}
}

func (app *application) getFiles(w http.ResponseWriter, r *http.Request) {
	auth := app.getAuth(r)
	if auth == nil {
		app.errServer(w, r, fmt.Errorf("missing auth on context"))
		return
	}

	folders, err := app.files.GetFoldersByOwner(auth.owner)
	if err != nil {
		app.errServer(w, r, err)
		return
	}

	folderTable := make(map[int]*models.MediaFolder)

	for _, folder := range folders {
		folderTable[folder.ID] = folder
	}

	files, err := app.files.GetByOwner(auth.owner)
	if err != nil {
		app.errServer(w, r, err)
		return
	}

	fileTable := make(map[int64]*models.MediaPackage)

	for _, file := range files {
		fileTable[file.ID] = file

		if file.FolderID != nil {
			file.Folder = folderTable[*file.FolderID]
		}
	}

	items, err := app.files.GetItemsByOwner(auth.owner)
	if err != nil {
		app.errServer(w, r, err)
		return
	}

	for _, item := range items {
		file := fileTable[item.PackageID]
		if file == nil {
			continue
		}

		switch item.TypeCode {
		case "IMAGE_POSTER":
			if file.Poster != nil {
				if item.CreatedAt.After(file.Poster.CreatedAt) {
					file.Poster = item
				}
			} else {
				file.Poster = item
			}

		case "MEDIA_ORIG":
			if file.Media != nil {
				if item.CreatedAt.After(file.Media.CreatedAt) {
					file.Media = item
				}
			} else {
				file.Media = item
			}
		}
	}

	rm := rest.NewRestModel(app.config)

	if err := render.RenderList(w, r, rm.NewMediaPackageRespList(files)); err != nil {
		app.errServer(w, r, err)
		return
	}
}

func (app *application) getFolders(w http.ResponseWriter, r *http.Request) {
	auth := app.getAuth(r)
	if auth == nil {
		app.errServer(w, r, fmt.Errorf("missing auth on context"))
		return
	}

	folders, err := app.files.GetFoldersByOwner(auth.owner)
	if err != nil {
		app.errServer(w, r, err)
		return
	}

	rm := rest.NewRestModel(app.config)

	if err := render.RenderList(w, r, rm.NewMediaFolderRespList(folders)); err != nil {
		app.errServer(w, r, err)
		return
	}
}

func (app *application) newFolder(w http.ResponseWriter, r *http.Request) {
	auth := app.getAuth(r)
	if auth == nil {
		app.errServer(w, r, fmt.Errorf("missing auth on context"))
		return
	}

	data := &rest.MediaFolderReq{}
	if err := render.Bind(r, data); err != nil {
		app.logError(err)
		app.errInvalidRequest(w, r, err)
		return
	}

	folder := data.MediaFolder

	if utf8.RuneCountInString(folder.Name) < 1 || utf8.RuneCountInString(folder.Name) > 100 {
		app.errInvalidRequest(w, r, nil)
		return
	}

	folder.Owner = auth.owner

	err := app.files.NewFolder(folder)
	if err != nil {
		app.errServer(w, r, err)
		return
	}

	rm := rest.NewRestModel(app.config)

	render.Status(r, http.StatusCreated)
	if err := render.Render(w, r, rm.NewMediaFolderResp(folder)); err != nil {
		app.errServer(w, r, err)
		return
	}
}

func (app *application) updateFolder(w http.ResponseWriter, r *http.Request) {
	folder := r.Context().Value(contextKeyFolder).(*models.MediaFolder)

	data := &rest.MediaFolderReq{MediaFolder: folder}
	if err := render.Bind(r, data); err != nil {
		app.logError(err)
		app.errInvalidRequest(w, r, err)
		return
	}

	folder = data.MediaFolder

	if utf8.RuneCountInString(folder.Name) < 1 || utf8.RuneCountInString(folder.Name) > 100 {
		app.errInvalidRequest(w, r, nil)
		return
	}

	err := app.files.UpdateFolder(folder)
	if err != nil {
		app.errServer(w, r, err)
		return
	}

	rm := rest.NewRestModel(app.config)

	if err := render.Render(w, r, rm.NewMediaFolderResp(folder)); err != nil {
		app.errServer(w, r, err)
		return
	}
}

func (app *application) deleteFolder(w http.ResponseWriter, r *http.Request) {
	folder := r.Context().Value(contextKeyFolder).(*models.MediaFolder)

	err := app.files.DeleteFolder(folder)
	if err != nil {
		app.errServer(w, r, err)
		return
	}

	rm := rest.NewRestModel(app.config)

	if err := render.Render(w, r, rm.NewMediaFolderResp(folder)); err != nil {
		app.errServer(w, r, err)
		return
	}
}
