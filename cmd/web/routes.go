package main

import (
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
)

func (app *application) routes() http.Handler {
	r := chi.NewRouter()
	r.Use(secureHeaders)
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.URLFormat)
	r.Use(app.authenticate)

	r.Get("/", app.home)
	r.NotFound(app.home)

	r.Route("/api", func(r chi.Router) {
		r.Use(render.SetContentType(render.ContentTypeJSON))
		r.Use(app.requireAdmin)

		r.Route("/files", func(r chi.Router) {
			r.Route("/file/{fileID:^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$}", func(r chi.Router) {
				r.Use(app.fileCtx)
				r.Get("/", app.getFile)
			})
		})
	})

	r.Route("/files", func(r chi.Router) {
		r.Use(render.SetContentType(render.ContentTypeJSON))

		r.Use(app.requireAuthentication)

		r.Get("/", app.getFiles)

		r.Route("/file/{fileID:^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$}", func(r chi.Router) {
			r.Use(app.fileCtx)
			r.Get("/", app.getFile)
			r.Put("/", app.updateFile)
			r.Delete("/", app.deleteFile)
		})

		r.Route("/folder", func(r chi.Router) {
			r.Get("/", app.getFolders)
			r.Post("/new", app.newFolder)

			r.Route("/{folderID:^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$}", func(r chi.Router) {
				r.Use(app.folderCtx)
				r.Put("/", app.updateFolder)
				r.Delete("/", app.deleteFolder)
			})
		})
	})

	filesDir := http.Dir("./ui/static/")
	fileServer(r, "/static", filesDir)

	return r
}

func fileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit any URL parameters.")
	}

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(root))
		fs.ServeHTTP(w, r)
	})
}
