package main

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"github.com/go-chi/render"
	"zenogre.net/phoenix/uploader/pkg/util"
)

type errResponse struct {
	Err            error `json:"-"`
	HTTPStatusCode int   `json:"-"`

	StatusText string `json:"status"`
	AppCode    int64  `json:"code,omitempty"`
	ErrorText  string `json:"error,omitempty"`
}

type auth struct {
	valid bool
	admin bool
	owner string
}

func (app *application) isAdmin(r *http.Request) bool {
	auth := app.getAuth(r)
	if auth == nil {
		return false
	}

	return auth.admin
}

func (app *application) isAuthenticated(r *http.Request) bool {
	auth := app.getAuth(r)
	if auth == nil {
		return false
	}

	return auth.valid
}

func (app *application) checkAuth(r *http.Request, auth *auth) error {
	auth.owner = ""
	auth.valid = false

	user, pass, ok := r.BasicAuth()

	if !ok {
		return nil
	}

	ok, err := util.DoValidateFolder(app.config.APIUrl, user, pass)

	if err != nil {
		return err
	}

	if ok {
		auth.owner = user
		auth.valid = true
	}

	return nil
}

func (app *application) getAuth(r *http.Request) *auth {
	auth, ok := r.Context().Value(contextKeyAuth).(*auth)
	if !ok {
		return nil
	}

	return auth
}

func (app *application) logError(err error) {
	trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
	app.errorLog.Output(2, trace) //nolint
}

func (app *application) errNotFound(w http.ResponseWriter, r *http.Request) {
	if rerr := render.Render(w, r, &errResponse{
		HTTPStatusCode: http.StatusNotFound,
		StatusText:     http.StatusText(http.StatusNotFound),
	}); rerr != nil {
		app.logError(rerr)
	}
}

func (app *application) errInvalidRequest(w http.ResponseWriter, r *http.Request, err error) {
	if rerr := render.Render(w, r, &errResponse{
		HTTPStatusCode: http.StatusBadRequest,
		StatusText:     http.StatusText(http.StatusBadRequest),
	}); rerr != nil {
		app.logError(rerr)
	}
}

func (app *application) errServer(w http.ResponseWriter, r *http.Request, err error) {
	if err != nil {
		app.logError(err)
	}

	if rerr := render.Render(w, r, &errResponse{
		HTTPStatusCode: http.StatusInternalServerError,
		StatusText:     http.StatusText(http.StatusInternalServerError),
	}); rerr != nil {
		app.logError(rerr)
	}
}

func (app *application) errUnauthorized(w http.ResponseWriter, r *http.Request, err error) {
	if rerr := render.Render(w, r, &errResponse{
		HTTPStatusCode: http.StatusUnauthorized,
		StatusText:     http.StatusText(http.StatusUnauthorized),
	}); rerr != nil {
		app.logError(rerr)
	}
}

func (e *errResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}
