package main

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/gofrs/uuid"

	"zenogre.net/phoenix/uploader/pkg/models"
)

// Add security headers.
func secureHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-XSS-Protection", "1; mode=block")
		w.Header().Set("X-Frame-Options", "deny")

		next.ServeHTTP(w, r)
	})
}

// Require the user to be an admin.
func (app *application) requireAdmin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !app.isAdmin(r) {
			app.errUnauthorized(w, r, nil)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// Require the user to be authenticated.
func (app *application) requireAuthentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !app.isAuthenticated(r) {
			app.errUnauthorized(w, r, nil)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// Authenticate request and check admin status.
func (app *application) authenticate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		auth := &auth{
			valid: false,
			admin: false,
			owner: "",
		}

		var err error
		realIP := r.Header.Get("X-Real-IP")
		if realIP == "" {
			realIP, _, err = net.SplitHostPort(r.RemoteAddr)
			if err != nil {
				app.errServer(w, r, fmt.Errorf("userip: %q is not IP:port: %w", r.RemoteAddr, err))
				return
			}
		}

		app.files.AuditUser = realIP

		for _, checkIP := range app.config.AdminIPS {
			if checkIP == realIP {
				auth.admin = true
				break
			}
		}

		err = app.checkAuth(r, auth)
		if err != nil {
			app.errServer(w, r, err)
			return
		}

		if !auth.valid && !auth.admin {
			next.ServeHTTP(w, r)
			return
		}

		ctx := context.WithValue(r.Context(), contextKeyAuth, auth)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Validate, authenticate, and attach file date to context.
func (app *application) fileCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fileID := chi.URLParam(r, "fileID")

		if fileID == "" {
			app.errNotFound(w, r)
			return
		}

		id, err := uuid.FromString(fileID)
		if err != nil {
			app.infoLog.Printf("failed to decode id from filename %q: %v", fileID, err)
			app.errNotFound(w, r)
			return
		}

		file, err := app.files.GetByID(id)
		if errors.Is(err, models.ErrNoRecord) {
			app.errNotFound(w, r)
			return
		} else if err != nil {
			app.errServer(w, r, err)
			return
		}

		auth := app.getAuth(r)
		if auth != nil {
			if !auth.admin && file.Owner != auth.owner {
				app.errUnauthorized(w, r, nil)
				return
			}
		}

		items, err := app.files.GetItemsByPackage(file.ID)
		if err != nil {
			app.errServer(w, r, err)
			return
		}

		for _, item := range items {
			switch item.TypeCode {
			case "IMAGE_POSTER":
				if file.Poster != nil {
					if item.CreatedAt.After(file.Poster.CreatedAt) {
						file.Poster = item
					}
				} else {
					file.Poster = item
				}

			case "MEDIA_ORIG":
				if file.Media != nil {
					if item.CreatedAt.After(file.Media.CreatedAt) {
						file.Media = item
					}
				} else {
					file.Media = item
				}
			}
		}

		if file.FolderID != nil {
			folder, err := app.files.GetFolderByID(*file.FolderID)

			if err != nil {
				app.errServer(w, r, err)
				return
			}

			file.Folder = folder
		}

		ctx := context.WithValue(r.Context(), contextKeyFile, file)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Validate, authenticate, and attach folder data to context.
func (app *application) folderCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		folderID := chi.URLParam(r, "folderID")

		if folderID == "" {
			app.errNotFound(w, r)
			return
		}

		id, err := uuid.FromString(folderID)
		if err != nil {
			app.infoLog.Printf("failed to decode id from folder %q: %v", folderID, err)
			app.errNotFound(w, r)
			return
		}

		folder, err := app.files.GetFolderByID(id)
		if errors.Is(err, models.ErrNoRecord) {
			app.errNotFound(w, r)
			return
		} else if err != nil {
			app.errServer(w, r, err)
			return
		}

		auth := app.getAuth(r)
		if auth != nil {
			if folder.Owner != auth.owner {
				app.errUnauthorized(w, r, nil)
				return
			}
		}

		ctx := context.WithValue(r.Context(), contextKeyFolder, folder)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
