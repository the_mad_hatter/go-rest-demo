package main

import (
	"flag"
	"log"
	"net/http"
	"os"

	"zenogre.net/phoenix/uploader/pkg/models/pgsql"
	"zenogre.net/phoenix/uploader/pkg/util"
)

type contextKey string

const contextKeyAuth = contextKey("auth")
const contextKeyFile = contextKey("file")
const contextKeyFolder = contextKey("folder")

type application struct {
	errorLog *log.Logger
	infoLog  *log.Logger
	files    *pgsql.DBModel
	config   *util.Config
}

func main() {
	addr := flag.String("addr", ":4000", "HTTP network address")
	confPath := flag.String("conf", os.Getenv("PNX_CONFIG"), "Config File")
	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	conf, err := util.LoadConfig(*confPath)
	if err != nil {
		errorLog.Fatalf("unable to load config: %v", err)
	}

	db, err := util.OpenDBPool(conf.DSN)
	if err != nil {
		errorLog.Fatal(err)
	}

	defer db.Close()

	dbmodel := pgsql.NewDBModel(db)

	app := &application{
		errorLog: errorLog,
		infoLog:  infoLog,
		files:    dbmodel,
		config:   conf,
	}

	srv := &http.Server{
		Addr:     *addr,
		ErrorLog: errorLog,
		Handler:  app.routes(),
	}

	infoLog.Printf("Starting server on %s", *addr)
	err = srv.ListenAndServe()
	errorLog.Fatal(err)
}
