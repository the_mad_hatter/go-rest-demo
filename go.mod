module zenogre.net/phoenix/uploader

go 1.14

require (
	github.com/aws/aws-sdk-go v1.32.13
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/jackc/pgconn v1.6.1
	github.com/jackc/pgtype v1.4.0
	github.com/jackc/pgx/v4 v4.7.1
	github.com/pkg/sftp v1.11.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	gopkg.in/vansante/go-ffprobe.v2 v2.0.1
)
